<?php if(!$GLOBALS['domain']) exit;?>

<footer role="contentinfo" class="small pxt">

	<div class="mw1140p center grid md:grid-2 space-l pys">

		<?txt('coordonnees')?>

		<div class="grid md:grid-2 space-l">

			<div><?txt('reseaux')?></div>
	
			<div><?txt('liens-utiles')?></div>

		</div>
		
	</div>
	
	<div class="mod w100 tc"><?txt('webmaster')?></div>

</footer>