<?php if(!$GLOBALS['domain']) exit;?>

<section class="grid md:grid-home pbl">

	<div id="head" class="banniere bg-primary flex flex-column jcc">

		<div class="banniere--image relative mod">
			<?media('visuel',['class' => 'w100', 'size'=>'1330', 'lazy'=>true, 'srcset'=>[320,640,960,1330]])?>
		</div>

		<div class="banniere--texte pxt md:pxm pyl auto">

			<?h1('titre', 'mw1140p mtn bold')?>
			<?php txt('txt','mw1140p block li-special')?>

			<div class="mtl prs tr">
				<a <?href('bt-catalogue')?> class="mbt inbl bt-secondary <?=(!@$content['bt-catalogue-texte']?'editable-hidden':'')?>"><?span('bt-catalogue-texte')?></a>
				<a <?href('bt-rdv')?> class="inbl bt-secondary <?=(!@$content['bt-rdv-texte']?'editable-hidden':'')?>"><?span('bt-rdv-texte')?></a>
			</div>

		</div>

	</div>

	<div id="main" class="mw1920p center pxt">

		<article class="pyt">

			<?h2('concept', 'pt-header')?>

			<div class="concept grid md:grid-3 gxs">

				<div class="concept__element mw960p separate md:prl pys">
					<div><?h3('concept-titre-1','ico-circulaire')?></div>	
					<?txt('concept-texte-1','')?>
					<?txt('concept-liste-1','concept__element-liste li-special')?>
				</div>

				<div class="concept__element mw960p separate md:prl pys">
					<div><?h3('concept-titre-2','ico-re')?></div>
					<?txt('concept-texte-2','')?>
					<?txt('concept-liste-2','concept__element-liste li-special')?>
				</div>

				<div class="concept__element mw960p separate md:prl pys">
					<div><?h3('concept-titre-3','ico-client')?></div>
					<?txt('concept-texte-3','')?>
					<?txt('concept-liste-3','concept__element-liste li-special')?>
				</div>

			</div>

		</article>

		<article class="pyt">

			<?php
				$module = module("cycle");
			?>

			<?h2('cycles', 'pt-header')?>

			<ul id="cycle" class="module unstyled pan grid md:grid-3 gxs">
			<?php
			foreach($module as $key => $val)
			{
				?>
				<li data-cycle="<?=$key?>" class="cycle mw640p separate md:prl pys">

					<div class="cycle--titre">
						<?h3('cycle-titre-'.$key, 'mbn mbl tdn mark')?>
					</div>

					<div class="cycle--texte">
						<?txt('cycle-texte-'.$key,['class'=>'mbs li-special','tag'=>'p'])?>
					</div>

					<div class="cycle--images">
						<div class="relative">
							<?media('cycle-img-1-'.$key,['class' =>'w100', 'size'=>'640', 'lazy'=>true])?>
							<?media('cycle-img-2-'.$key,['class' =>'w100 editable-hidden', 'size'=>'640', 'lazy'=>true])?>
							<?media('cycle-img-3-'.$key,['class' =>'w100 editable-hidden', 'size'=>'640', 'lazy'=>true])?>
							<?media('cycle-img-4-'.$key,['class' =>'w100 editable-hidden', 'size'=>'640', 'lazy'=>true])?>
							<button class="bt-primary small" aria-controls="viewer" aria-expanded="false"><span>Plus d'images</span></button>
						</div>
					</div>

					<ul class="cycle--indicateurs unstyled pan mtl mbl">

						<li class="grid grid-2 <?=(!@$val['valeur-1']?' editable-hidden':'')?> mbt">
							<label for="meter-1-<?=$key?>">Confort et ergonomie</label>
							<?input('cycle-meter-confort-'.$key,['type'=>'number','class'=>'w20p editable-hidden'])?>
							<meter id="meter-1-<?=$key?>" min="0" max="4" value="<?=@$val['confort']?>">score : <?=@$val['confort']?> sur 4</meter>
						</li>

						<li class="grid grid-2 <?=(!@$val['valeur']?' editable-hidden':'')?> mbt">
							<label for="meter-2-<?=$key?>">Performance</label>
							<?input('cycle-meter-performance-'.$key,['type'=>'number','class'=>'w20p editable-hidden'])?>
							<meter id="meter-2-<?=$key?>" min="0" max="4" value="<?=@$val['performance']?>">score : <?=@$val['performance']?> sur 4</meter>
						</li>

						<li class="grid grid-2 <?=(!@$val['valeur']?' editable-hidden':'')?> mbt">
							<label for="meter-3-<?=$key?>">Robustesse</label>
							<?input('cycle-meter-robustesse-'.$key,['type'=>'number','class'=>'w20p editable-hidden'])?>
							<meter id="meter-3-<?=$key?>" min="0" max="4" value="<?=@$val['robustesse']?>">score : <?=@$val['robustesse']?> sur 4</meter>
						</li>
						
					</ul>

					<div class="cycle--details">
						
						<?txt('cycle-details-'.$key,'details is-closed mbm')?>

						<button id="cycle-bt-<?=$key?>" type="button" class="toogle-button a" aria-expanded="false" aria-controls="<?='cycle-details-'.$key?>">
							<span class="when-close">Afficher les détails</span>
							<span class="when-open">Masquer les détails</span>
						</button>
					</div>

				</li>
				<?php
			}
			?>
			</ul>

			<aside class="mw1140p center pyt mtl">

				<?txt('accessoires-textes','bg-primary pam li-special')?>

				<div class="mtl prs tr">
					<a <?href('bt-url')?> class="inbl bt-secondary"><?span('bt-texte')?></a>
				</div>

			</aside>

		</article>

		<article class="pyt">

			<?h2('engagements', 'pt-header')?>

			<?h3('engagement-titre-1','mark')?>

			<div class="grid gxs md:grid-2">

				<?media('engagement-img-1',['class' => 'w100', 'size' => '500'])?>

				<?txt('engagement-texte-1','block li-special')?>

			</div>
			
			<?h3('engagement-titre-2','mark')?>

			<div class="grid gxs md:grid-2 md:grid-2-colswap">

				<?media('engagement-img-2',['class' => 'w100', 'size' => '500'])?>

				<?txt('engagement-texte-2','block li-special')?>

			</div>

		</article>


	</div>

</section>

<?include_once("contact.php");?>

<script src="/theme/<?=$GLOBALS['theme']?>/function<?=$GLOBALS['min']?>.js"></script>