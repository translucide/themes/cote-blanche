<?php
if(!$GLOBALS['domain']) exit;
if(!@$GLOBALS['content']['titre']) $GLOBALS['content']['titre'] = $GLOBALS['content']['title'];
?>

<section class="mw1140p center pyt pxt mtl">

	<figcation class="mw1140p block mbm">
		<?media('visuel',['class' => 'mw100', 'size' => '1140x480', 'crop' => true, 'srcset' => [320, 640,960,1140]])?>
		<?txt('legende', ['class' => 'small color-primary italic','tag' => 'figcaption'])?>
	</figcation>

	<?php h1('titre', 'color-primary')?>

	<aside class="mbm">

		<? 

		$date = explode("-", explode(" ", $res['date_insert'])[0]);

		?>

		<div class="inbl mbt">
			<?_e('Publié le')?>: <time class="" datetime="<?=$res['date_insert']?>"><?=strftime('%d %B %Y', strtotime(@$res['date_insert']))?></time>
			—
			<?_e('Mis à jour le')?>: <time class="" datetime="<?=$res['date_insert']?>"><?=strftime('%d %B %Y', strtotime(@$res['date_insert']))?></time>
		</div>

		<div class="inbl <?=(!@$content['actualites']?'editable-hidden':'')?>">
			<span class="inbl bold"><?php _e("Catégories")?> : </span>	
			<?php tag('actualites')?>
		</div>

	</aside>

	<article class="pyt">


		<?php txt('texte', ['class' => '','dir'=>'actu'])?>

	</article>
	
	<?php 
	// Retour à la liste des actualités
	$sql = 'SELECT * from '. $tc .' WHERE tpl = \'article-liste\' LIMIT 1';
	$sel = $connect->query($sql);
	$parent = $sel->fetch_all(MYSQLI_ASSOC);

	print '<aside class="tc"><a href="'.make_url($parent[0]['url'], ['domaine' => true]).'" class="bt-secondary"><span>'. __('Découvrir nos actualités').'</span></a></aside>';
	?>

</section>
