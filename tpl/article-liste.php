<?php  
	if(!$GLOBALS['domain']) exit; 

	/**
	 * Controle des droits d'éditions
	 */
	(!@$_SESSION['auth']['edit-article'] ? $sql_state = "AND state='active'" : $sql_state = "");

	/**
	 * Liste des tags
	 */
	$tags_list = $connect->query("SELECT distinct encode, name FROM ".$table_tag." WHERE zone='".$res['url']."' ORDER BY ordre ASC, encode ASC");
	$tags = $sel->fetch_all(MYSQLI_ASSOC);

	
	/**
	 * Liste des articles
	 */

	/** si filtre tag */
	( isset($tag) ? $sql_tag=" RIGHT JOIN ".$tt." ON ( ".$tt.".id = ".$tc.".id AND ".$tt.".zone = 'actualites' AND ".$tt.".encode = '".$tag."' )" : null );
	
	/** pagination */
	$num_pp = 10;

	/** préparation de la requête */
	$sql_select ="SELECT SQL_CALC_FOUND_ROWS ".$tc.".id, ".$tc.".* FROM ".$tc
	. @$sql_tag 
	. " WHERE (".$tc.".type='article') AND ".$tc.".lang='".$lang."' ".$sql_state;

	if(isset($GLOBALS['filter']['page'])) {

		$page = (int)$GLOBALS['filter']['page'];
		$start = ($page * $num_pp) - $num_pp;

	} 
	else {
		$page = $start = 0;

		/** Selection de l'acutalité la plus récente */
		$sql = $sql_select ." ORDER BY ".$tc.".date_insert DESC LIMIT 1";
		$sel = $connect->query($sql);
		$spotlight = $sel->fetch_all(MYSQLI_ASSOC);
		$excepted = $spotlight[0]['id'];

	}

	/**
	 * Selection des acutalités de façon aléatoire
	 */ 
	$sql = $sql_select
	. ( isset($excepted) ? " AND ". $tc .".id <> ". $excepted : null )
	. ( $page == 0 ? " ORDER BY RAND()" : " ORDER BY ".$tc.".date_insert DESC" ) 
	. " LIMIT ".$start.",".$num_pp;

	$sel = $connect->query($sql);
	$news = $sel->fetch_all(MYSQLI_ASSOC);
	$num_total = $connect->query("SELECT FOUND_ROWS()")->fetch_row()[0];

	/**
	 * Union des requêtes pour la page 0
	 */
	@$spotlight ? $news = array_merge($spotlight, $news) : null;


?>

<section class="mw1140p center pxt">

	<?php h1('title', 'color-primary mbn')?>

	<nav role="navigation" class="mts italic">
	
		<?php 
		/**
		 * Affichage des tags
		 */
		$tags_list->num_rows ? _e("Catégories: ") : null;

		echo'<ul class="tags-list unstyled pan man inbl">';
		foreach ($tags_list as $key => $val) {
			echo'<li class="inbl plt tags__item"><a href="'.make_url($res['url'], array($val['encode'], 'domaine' => true)).'" class="color tdn dash">'.$val['name'].'</a>'.( $key < $tags_list->num_rows - 1 ? ',' : null ).'</li>';
		}
		echo'</ul>';
		?>

	</nav>

	<div class="blog <?=($page > 0 || @$tag ? "blog--pagination" : "blog--spotlight");?> mtl">

		<?

	/**
	 * Affichage des articles
	 */
	
	foreach($news as $key => $val)
	{
		
		// Affichage du message pour dire si l'article est invisible ou pas
		if($val['state'] != "active") $state = " <span class='deactivate pat'>".__("Article d&eacute;sactiv&eacute;")."</span>";
		else $state = "";

		$content_fiche = json_decode((string)$val['content'], true);

		$date = explode("-", explode(" ", $val['date_insert'])[0]);

		$page < 2 && $key <= 2 && !@$tag ? $spotlight = true : $spotlight = false;

		?>
		<article class="blog__topic <?=($spotlight ? "blog__topic--spotlight" : null) . ($key == 3 ? "mtl ptm" : null)?>">

				<?php
				if($spotlight && @$content_fiche['visuel']) {

					$img_actu = $content_fiche['visuel'];

					// Crée une minature si l'image est trop grande
					$img_actu_clean = parse_url($img_actu, PHP_URL_PATH);

					list($source_width, $source_height) = getimagesize($img_actu_clean);// Taille de l'image

					if($key==0) { //grande miniature
						$new_width = 1140;// Largeur max
						$new_height = 480;// hauteur max
					}
					else {
						$new_width = 720;// Largeur max
						$new_height = 360;// hauteur max
					}

					// Image a resize ?
					if($source_width > $new_width)
					{
						// Nom de l'image resize pour voir si elle existe
						if(!isset($new_height)) $new_height = round($new_width * $source_height / $source_width);// Nouvelle hauteur
						preg_match("/(-[0-9]+x[0-9]+)\./", $img_actu_clean, $matches);// Pour modifier le nom de l'image avec la nouvelle taille

						if(isset($matches[1])) $new_name = str_replace($matches[1], "-".$new_width."x".$new_height, $img_actu_clean);
						else
						{// Cas d'une image pas forcement redimentionner lors de l'upload initial
							$pathinfo = pathinfo($img_actu_clean);
							$new_name = $pathinfo['dirname'].'/'.$pathinfo['filename']."-".$new_width."x".$new_height.$pathinfo['extension'];
						}

						// La nouvelle image existe déjà ?
						if(file_exists($new_name)) $img_actu = $new_name;							
						else $img_actu = resize($img_actu_clean, $new_width, $new_height, null, 'crop');
					}

				?>

					<figure>
						<img src="<?=$img_actu?>" alt="<?=@$content_fiche['visuel-alt']?>">
						<?@$content_fiche['legende'] ? '<figcaption>'.$content_fiche['legende'].'</figcaption>' : null;?>
					</figure>
				
				<?
				}
				?>

				<h2 class="">
					<a href="<?=make_url($val['url'], array("domaine" => true));?>" class="tdn"><?=$val['title']?></a><?=$state?>
				</h2>
		
				<?php
				if($spotlight) {?>

					<aside class="mbm">
						<span class="bold"><?_e('Publié le')?>:</span> <time class="" datetime="<?=$res['date_insert']?>"><?=strftime('%d %B %Y', strtotime(@$res['date_insert']))?></time>
					</aside>

					<p class="">
						<?php if(isset($content_fiche['texte'])) echo word_cut($content_fiche['texte'], '350')."…";?>
					</p>

					<div>
						<a href="<?=make_url($val['url'], array("domaine" => true));?>" class="bt-secondary" title="Lire l'article : <?=$val['title']?>">
						<span><?php _e("Lire l'article")?></span>
						</a>
					</div>
				
				<?
				}
				?>

		</article>
		<?php 
	}
	?>
	</div>
</section>