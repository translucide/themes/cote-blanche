<?php 

switch(@$_GET['mode'])
{
	// AFFICHAGE DU FORMULAIRE DE CONTACT
	default :

		if(!$GLOBALS['domain']) exit;		
		?>


		<script>
		add_translation({
			"Thank you for completing all the required fields!" : {"fr" : "Merci de remplir tous les champs obligatoires !"},
			"Wrong answer to the verification question!" : {"fr" : "R\u00e9ponse erron\u00e9e \u00e0 la question de vérification !"},
			"Error sending email" : {"fr" : "Erreur lors de l'envoi du mail"},
			"Invalid email!" : {"fr" : "Mail invalide !"},
			"Message sent" : {"fr" : "Message envoy\u00e9"},
		});
		</script>
		

		<div class="bg-secondary pys">
			
			<form id="frm-contact" class="mw1140p center pxt pys" >

				<?php h2('contact','mtn pt-header')?>		
				<?php txt('texte', '')?>

				<?php txt('texte-champs-obligatoires', 'mbm')?>

				<div class="mbm">
					<label for="email-from" class="block"><?php span('texte-label-email')?><span>*</span></label>
					<input type="email" name="email-from" id="email-from" autocomplete="email" class="w40 vatt bg-transparent" required>

					<label for="reponse" class="hidden" aria-hidden="true"><?php _e("Email")?></label>
					<input name="reponse" id="reponse" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$" placeholder="nom@domaine.com" aria-hidden="true" class="hidden">
				</div>

				<div class="mbm">
					<label for="tel-from" class="block"><?php span('texte-label-tel')?></label>
					<input type="tel" name="tel-from" id="tel-from" autocomplete="tel" class="w40 vatt bg-transparent">
				</div>

				<div class="mbm">
					<label for="object" class="block"><?php span('texte-label-object')?><span>*</span></label>
					<select name="object" id="object"  class="w40 vatt bg-transparent" required>
						<option value="reserver">Réserver un vélo</option>
						<option value="information">Demande information</option>
						<option value="partenaire">Devenir partenaire</option>
						<option value="autre" selected>Autre</option>
					</select>
				</div>

				<div>
					<label for="message" class="block"><?php span('texte-label-message')?><span>*</span></label>
					<textarea name="message" id="message" class="w100 mbt bg-transparent" style="height: 200px;" required></textarea>
				</div>

				<div class="mtl">

					<!-- Question -->
					<?
					$chiffre = array('zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten');
					$operators = array("+", "-");
					$operator = $operators[array_rand($operators)];
					$nb1 = rand(1, 5);//10
					$nb2 = ($operator === '-') ? mt_rand(1, $nb1) : mt_rand(1, 5);// on évite les résultats négatifs en cas de soustraction
					eval('$question = strval('.$nb1.$operator.$nb2.');');
					$question_hash = hash('sha256', $question.$GLOBALS['pub_hash']);
					// On change le signe "-" moins de calcul en "−" lisible en accessibilité
					?>
					<div>
						<label for="question">
							<?php span('texte-label-question')?>:
							<?=(__($chiffre[$nb1])." ".($operator=='-'?'−':$operator)." ".__($chiffre[$nb2]));?><span>*</span> = 
						</label>
						<input type="text" name="question" id="question" placeholder="?" class="w50p tc black" autocomplete="off" required>

						<input type="hidden" name="question_hash" value="<?=$question_hash;?>">
					</div>

					<!-- RGPD -->
					<div class="mtm">
						<label for="rgpdcheckbox" class="inline" style="text-transform: none;"><?php span('rgpd')?><span>*</span></label>
						<input type="checkbox" name="rgpdcheckbox" id="rgpdcheckbox" required>
					</div>

				</div>

				<div class="tr mtm prs">
					<button type="submit" id="send" class="bt-primary">
						<span><?php _e(array("Send" => array("fr" => "Envoyer")))?></span>
					</button>
				</div>

				<input type="hidden" name="rgpd_text" value="<?=htmlspecialchars(@$GLOBALS['content']['rgpd']);?>">

				<input type="hidden" name="nonce_contact" value="<?=nonce("nonce_contact");?>">

				<input type="hidden" name="referer" value="<?=htmlspecialchars((isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:""));?>">

			</form>

		</div>

		<script>
			// Titre de la page en cours
			title = document.title;

			// Pour rétablir le fonctionnement du formulaire
			function activation_form(){
				desactive = false;

				$("#frm-contact #send .fa-cog").removeClass("fa-spin fa-cog").addClass("fa-mail-alt");

				// Activation des champs du formulaire
				$("#frm-contact input, #frm-contact textarea, #frm-contact button").removeClass("disabled");//readonly .attr("aria-disabled", true)

				// On peut soumettre le formulaire avec la touche entrée
				//$("#frm-contact").on("submit", function(event) { send_mail(event) });
				$("#frm-contact button").attr("aria-disabled", false);
			}

			desactive = false;
			function send_mail(event)
			{
				event.preventDefault();

				if($("#question").val()=="" || $("#message").val()=="" || $("#email-from").val()=="" || $("#rgpdcheckbox").prop("checked") == false)
				error(__("Thank you for completing all the required fields!"));
				else if(!desactive)
				{
					desactive = true;

					// Icone envoi en cours
					$("#frm-contact #send .fa-mail-alt").removeClass("fa-mail-alt").addClass("fa-spin fa-cog");

					// Désactive le formulaire
					$("#frm-contact input, #frm-contact textarea, #frm-contact button").addClass("disabled");//readonly .attr("aria-disabled", true)

					// Désactive le bouton submit (pour les soumissions avec la touche entrée)
					//$("#frm-contact").off("submit");
					$("#frm-contact button").attr("aria-disabled", true);// => ne permet pas le focus sur le bt une fois envoyer

					$.ajax(
						{
							type: "POST",
							url: path+"theme/"+theme+(theme?"/":"")+"tpl/contact.php?mode=send-mail",
							data: $("#frm-contact").serializeArray(),
							success: function(html){ $("body").append(html); }
						});
				}
			}

			$(function()
			{
				// Message d'erreur en cas de mauvaise saisie du mail. Pour l'accessibilité
				var email_from = document.getElementById("email-from");
				email_from.addEventListener("invalid", function() {
					email_from.setCustomValidity("<?_e("Invalid email")?>. <?_e("Expected format")?> : dupont@exemple.com")
				}, false);
				email_from.addEventListener("input", function() {
					email_from.setCustomValidity("");
				}, false);
				
				// Soumettre le formulaire
				$("#frm-contact").submit(function(event)
				{
					send_mail(event)
				});
			});
		</script>
		<?php 

	break;



	// SCRIPT D'ENVOIE DE L'EMAIL
	case 'send-mail':

		//print_r($_REQUEST);

		// Si on a posté le formulaire
		if(isset($_POST["email-from"]) and $_POST["message"] and isset($_POST["question"]) and isset($_POST["object"]) and !$_POST["reponse"])// reponse pour éviter les bots qui remplisse tous les champs
		{
			include_once("../../../config.php");// Les variables

			if($_SESSION["nonce_contact"] and $_SESSION["nonce_contact"] == $_POST["nonce_contact"])// Protection CSRF
			{
				if(filter_var($_POST["email-from"], FILTER_VALIDATE_EMAIL))// Email valide
				{
					if(hash('sha256', $_POST["question"].$GLOBALS['pub_hash']) == $_POST["question_hash"])// Captcha valide
					{
						$from = ($_POST["email-from"] ? htmlspecialchars($_POST["email-from"]) : $GLOBALS['email_contact']);


						$subject = "[".htmlspecialchars($_SERVER['HTTP_HOST'])."] ".htmlspecialchars($_POST["object"]);


						// Message
						$message = (strip_tags($_POST["message"]));
						$message.= "\n\ncontact:\nmail:".$_POST['email-from']."\ntéléphone:".$_POST['tel-from'];

						$message .= "\n\n-------------------------------------------------------\n";

						if($_POST['referer']) $message .= "Referer : ".htmlspecialchars($_POST['referer'])."\n";

						$message .= "Consentement : ".htmlspecialchars($_POST["rgpd_text"])."\n";
						$message .= "IP du Visiteur : ".getenv("REMOTE_ADDR")."\n";
						$message .= "Host : ".gethostbyaddr($_SERVER["REMOTE_ADDR"])."\n";
						$message .= "IP du Serveur : ".getenv("SERVER_ADDR")."\n";
						$message .= "User Agent : ".getenv("HTTP_USER_AGENT")."\n";


						// header
						$header = "From:".$GLOBALS['email_contact']."\r\n";// Pour une meilleure délivrabilité des mails
						$header.= "Reply-To: ".$from."\r\n";
						$header.= "Content-Type: text/plain; charset=utf-8\r\n";// utf-8 ISO-8859-1


						if(mail($GLOBALS['email_contact'], $subject, stripslashes($message), $header))
						{
							?>
							<script>
								popin(__("Message sent"), 'nofade', 'popin', $("#send"));
								document.title = title +' - '+ __("Message sent");

								// Icone envoyer
								$("#frm-contact #send .fa-spin").removeClass("fa-spin fa-cog").addClass("fa-ok");
							</script>
							<?php 
						}
						else {
							?>
							<script>
								error(__("Error sending email"), 'nofade', $("#send"));
								document.title = title +' - '+ __("Error sending email");
								
								activation_form();// On rétablie le formulaire
							</script>
							<?php 
							//echo error_get_last()['message']; print_r(error_get_last());
						}
					}
					else
					{
						?>
						<script>
							error(__("Wrong answer to the verification question!"), 'nofade', $("#question"));
							document.title = title +' - '+ __("Wrong answer to the verification question!");
							
							activation_form();// On rétablie le formulaire
						</script>
						<?php 
					}
				}
				else
				{
					?>
					<script>
						error(__("Invalid email!"), 'nofade', $("#email-from"));
						document.title = title +' - '+ __("Invalid email!");
						
						activation_form();// On rétablie le formulaire
					</script>
					<?php 
				}
			}
		}

	break;
}
?>