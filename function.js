document.querySelectorAll('#cycle button').forEach(function(button){
    button.addEventListener('click',function(event){
        event.preventDefault();
        
        var control = button.getAttribute('aria-controls');

        expanded = button.getAttribute('aria-expanded') === 'true' ? 'false' : 'true';
        button.setAttribute('aria-expanded', expanded);

		
		switch (control) {
			// visionneuse d'images
			case 'viewer':

				// récupération des sources d'images
				var key = button.closest('li').dataset.cycle;
				var cycle_imgs = document.querySelectorAll('[data-cycle="'+key+'"] .cycle--images img');
				var cycle_srcs = [];
				var cycle_srcs_alt = [];
				cycle_imgs.forEach(function(cycle_img){

					if(cycle_img.src.includes('?zoom='))
						cycle_srcs.push(cycle_img.src.split("?zoom=")[1].trim().split("&")[0]);
					else 
						cycle_srcs.push(cycle_img.src.split("?")[0]);

					cycle_srcs_alt.push(cycle_img.alt);
				});

				// selection de l'image grande taille
				
				// création de la popin
				var viewer = document.createElement('div');
				viewer.id="viewer";
				viewer.setAttribute('data-cycle',key);
				viewer.setAttribute('tabindex','-1');
				viewer.classList.add('layerZoom');
				viewer.innerHTML += 
				'<div class="layerZoom--container pas">'
				+'<img src="'+cycle_srcs[0]+'" alt="'+cycle_srcs_alt[0]+'" data-id="cycle-img-1-1" data-srcs="'+cycle_srcs+'" class="w80">'
				+'<nav role="navigation" class="layerZoom--container-nav">'
				+'<ul class="unstyled ">'
				+'<li><button class="zoom--prev bt-secondary" title="Précédent" aria-label="Image précédente"><span class="fa fa-up-open" aria-hidden="true"></span></button></li>'
				+'<li><button class="zoom--next bt-secondary" title="Suivant" aria-label="Image suivante"><span class="fa fa-up-open" aria-hidden="true"></span></button></li>'
				+'</ul>'
				+'</nav>'
				+'</div>'
				+'<button class="zoom-close" aria-label="Fermer"><div class="bt-secondary fa fa-cancel" aria-hidden="true"></div></button>'
				document.body.querySelector('main').appendChild(viewer);
				viewer.focus();

				//on désactive les boutons si en début et fin d'images
				var selImg = document.querySelector('.layerZoom .layerZoom--container > img').dataset.id
				var numImg = parseInt(selImg.match(/([0-9]+)(?=-)/)[0]);
				if(numImg == 1) 
					document.querySelector('.layerZoom .layerZoom--container .zoom--prev').disabled = true
				
				if(numImg == cycle_imgs.length)
				document.querySelector('.layerZoom .layerZoom--container .zoom--next').disabled = true

				// navigation entre les images
				document.querySelectorAll('.layerZoom .layerZoom--container .zoom--prev, .layerZoom .layerZoom--container .zoom--next').forEach(function(zoomButton){

					zoomButton.addEventListener('click',function(event){

						event.preventDefault();

						var selImg = document.querySelector('.layerZoom .layerZoom--container > img').dataset.id
						var numImg = parseInt(selImg.match(/([0-9]+)(?=-)/)[0]);

						// image précédente
						if(zoomButton.classList.contains('zoom--prev') && numImg > 1) 
							newId = numImg - 1;

						// image suivante
						if(zoomButton.classList.contains('zoom--next') && numImg < cycle_imgs.length) 
							newId = numImg + 1;

						document.querySelector('.layerZoom .layerZoom--container > img').src = document.querySelector('.layerZoom .layerZoom--container> img').dataset.srcs.split(',')[newId - 1];
						document.querySelector('.layerZoom .layerZoom--container > img').dataset.id = selImg.replace(/([0-9]+)(?=-)/,newId.toString());
						
						//on désactive les boutons si en début et fin d'images
						if(newId == 1) 
							document.querySelector('.layerZoom .layerZoom--container .zoom--prev').disabled = true
						else
							document.querySelector('.layerZoom .layerZoom--container .zoom--prev').disabled = false

						if(newId == cycle_imgs.length)
							document.querySelector('.layerZoom .layerZoom--container .zoom--next').disabled = true
						else
							document.querySelector('.layerZoom .layerZoom--container .zoom--next').disabled = false
					});

				})

				// fermeture de la popin
				document.querySelector('.layerZoom .zoom-close').addEventListener('click', function() {
					expanded = button.getAttribute('aria-expanded') === 'true' ? 'false' : 'true';
        			button.setAttribute('aria-expanded', expanded);
					document.body.querySelector('main').removeChild(viewer);
				});

			
				break;
		
			default:
				break;
		}


        if(expanded === 'true') {
            document.querySelector('#'+control).classList.replace('is-closed','is-open');
        }
        else {
            document.querySelector('#'+control).classList.replace('is-open','is-closed');
        }
        

    });
});